#!/bin/bash

# Update package information
sudo apt-get update

# Install Apache web server
sudo apt-get install apache2 -y

# Start Apache service
sudo service apache2 start

# Clear contents of the default Apache directory
sudo rm -r /var/www/html/*

# Clone the Git repository and move its contents to the Apache directory
sudo git clone https://gitlab.com/devops722546/webpage /var/www/html/


# Ensure proper permissions for the web server
sudo chown -R www-data:www-data /var/www/html

# Restart Apache for changes to take effect
sudo service apache2 restart
